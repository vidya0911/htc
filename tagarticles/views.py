# Create your views here.
from tagarticles.forms import Article
from django.shortcuts import render_to_response , render
from django.template import RequestContext
import os , difflib

def index(request):
    article = Article()
    return render_to_response('tagarticles/index.html' , {'form':article} , context_instance = RequestContext(request))

def getarticles(request):
    if (request.method !='POST'):
	article = Article()
	return render_to_response('tagsarticles/index.html' , {'form':article},context_instance=RequestContext(request))
    else:
	article = request.POST['article']
	tagarticle(request , article)

def tagarticle(request):
    module_dir = os.path.dirname(__file__)  #current directoryfile
    tagfile = os.path.join(module_dir ,'tags/tags_stop_words.txt')
    taglist = open(tagfile, 'r').readlines()
    article = list(request.POST['article'])
    mytags = []
    import pdb ; pdb.set_trace()
    for tag in taglist:
	mytags.append(tag.rstrip('\n\r'))
    return matchtags(request , mytags , article)
    #import pdb ; pdb.set_trace()
    #return render_to_response('tagarticles/test.html' , {'data1':mytags , 'data2' : article},context_instance=RequestContext(request))

"""def matchtags(request , taglist , article):
    matchedtags = []
    for tag in taglist:
	if tag in article:
	    matchedtags.append(tag)
    return render_to_response('tagarticles/test.html' , {'data1':matchedtags,'data2':article},context_instance=RequestContext(request))"""

def matchtags(request , taglist , article):
    matchedtags = []
    for tag in taglist:
	if difflib.get_close_matches(tag,article):
	    matchedtags.append(difflib.get_close_matches)
    return render_to_response('tagarticles/test.html' , {'data1':matchedtags,'data2':article},context_instance=RequestContext(request))
