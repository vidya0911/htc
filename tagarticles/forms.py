from django import forms

class Article(forms.Form):
    article = forms.CharField(widget = forms.Textarea)
