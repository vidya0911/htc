from django.db import models
from django.contrib import auth
from django.contrib.auth.models import User

# Create your models here.

class Department(models.Model):
	name = models.CharField(max_length = 20)
	
	def __unicode__(self):
		return "Department : {0}".format(self.name)

class Designation(models.Model):
	name = models.CharField(max_length = 20 , verbose_name = 'Employee Designation') #verbose name is a human readable format
	
	def __unicode__(self):
		return "Designation : {0}".format(self.name)

class Employee(models.Model):
	name = models.ForeignKey(User , related_name = 'employee')
	manager = models.ForeignKey('Employee' ,null = True , blank = True , related_name = 'empmanager')
	department = models.ForeignKey(Department)
	designation = models.ForeignKey(Designation)
	
	def __unicode__(self):
		if self.manager is not None:
			return self.name.username
		else:
			return "Employee Id {0}: Designation : {1} {2}".format( self.name, 'CEO', self.department)
	
	def emp_name(self):
		return self.name
	
	def emp_email(self):
		return self.name.email

	def emp_manager(self):
		return self.manager
	
	def emp_department(self):
		return self.department.name

	def emp_designation(self):
		return self.designation.name

class Leave(models.Model):
	
	emp_leave = models.ForeignKey(Employee)
	casual = models.IntegerField()
	sick = models.IntegerField()
	privilege = models.IntegerField()
	paternity = models.IntegerField()
	
	def __unicode__(self):
		return "{0} Leave ".format(self.emp_leave)

class LeaveRecord(models.Model):
	employee = models.ForeignKey(Employee)
	LEAVE_TYPE = ( ('sick' , 'sick'), ('casual' , 'casual'), ('privilege' , 'privilege'), ('paternity' , 'paternity'),)
	leave_type = models.TextField(choices = LEAVE_TYPE , default = 'casual')
	start_date = models.DateField()
	end_date = models.DateField()
	reason = models.TextField()
	days = models.IntegerField(null = True , blank = True)
	grant = models.NullBooleanField(blank = True)
	application_date = models.DateField(null = True)

	def __unicode__(self):
		return "{0} Leave Application".format(self.employee)
	def name(self):
		return self.employee.name
