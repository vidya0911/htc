from django.contrib import admin
from manageleave.models import Department , Designation , Employee , Leave,LeaveRecord 
from django.contrib.auth.models import User
class LeaveInline(admin.TabularInline):
	model = Leave
	readonly_fields = ['emp_leave' , 'casual' , 'sick' , 'privilege' , 'paternity']
	extra = 1

class UserInline(admin.TabularInline):
	model = User
	#readonly_fields = ['emp_leave' , 'casual' , 'sick' , 'privilege' , 'paternity']
	extra = 1

class DesignationAdmin(admin.ModelAdmin):
	model = Designation

class EmployeeAdmin(admin.ModelAdmin):
	list_display = ['name','emp_email','emp_manager','emp_department','emp_designation']
#	search_fields = ['name'] # here as the search should be on the field of model so we have to put name here instead of any reference 
	list_filter = ['name']
	search_fields = ('name__username',)
	inlines = [LeaveInline,UserInline]
	#raw_id_fields = ('name',)
class LeaveRecordAdmin(admin.ModelAdmin):
	model = LeaveRecord
	list_display = ['name','leave_type','start_date','end_date','reason','days','grant','application_date']
	date_hierarchy = 'application_date'
	list_filter = ['application_date']


admin.site.register(Designation)
admin.site.register(Department)
admin.site.register(Leave)
admin.site.register(Employee,EmployeeAdmin)
admin.site.register(LeaveRecord,LeaveRecordAdmin)
