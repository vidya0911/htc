from django import forms
from django.contrib.admin import widgets      
from django.forms.extras.widgets import SelectDateWidget
from django.contrib.admin.widgets import AdminDateWidget 
from django.forms import ModelForm
from .models import LeaveRecord
from suit.widgets import AutosizedTextarea , SuitDateWidget
class LoginForm(forms.Form):
	username = forms.CharField(max_length = 10)
	password = forms.CharField(widget = forms.PasswordInput)

class LeaveForm(forms.Form):
	LEAVE_TYPE = (('' , '',),
			('casual','casual',) , 
				('sick', 'sick',),
				('privilege' , 'privilege',),
				('paternity' , 'paternity',),
			)
	startdate = forms.DateField()
	enddate = forms.DateField()
	leave = forms.ChoiceField(choices = LEAVE_TYPE)
	reason = forms.CharField(widget = forms.Textarea)

class LeaveApplication(ModelForm):
	class Meta:
		model = LeaveRecord
		widgets = {
				'reason': AutosizedTextarea(attrs={'rows': 2, 'class': 'input-xlarge'}),
        }
		fields = ('leave_type','start_date','end_date','reason',)
