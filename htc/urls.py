from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from views import home , elogin , profile , requestleave , thanks , logout , leaveapplication , leave_allocated , leave_status
from tagarticles.views import index , tagarticle
from api.views import postpara
from piston.resource import Resource
from api.handlers import TagArticleHandler
tagarticle_handler = Resource(TagArticleHandler)

urlpatterns = patterns('',
     url(r'^$',home),
     url(r'login/$',elogin),
     url(r'logout/$',logout),
     url(r'^profile/$',profile),
     url(r'^requestleave/$',requestleave),
     url(r'^leaveapplication/$',leaveapplication),
     url(r'^leave_allocated/$',leave_allocated),
     url(r'^leave_status/$',leave_status),
     #url(r'^validateleave/$',validateleave),
     #url(r'^validateleave/$',leaveapp),
     url(r'^thanks/$',thanks),
     url(r'^tags/$',index),
     url(r'^tagarticle/$',tagarticle),
     #url(r'^api/', include('api.urls')), 
     url(r'^api/tagarticles/$',tagarticle_handler), 
     url(r'^api/$',postpara), 
     
     # url for api
    # url(r'^htc/', include('htc.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
)
