from django.shortcuts import render_to_response , render
from django.http import HttpResponse , HttpRequest , HttpResponseRedirect
from manageleave.forms import LoginForm , LeaveForm , LeaveApplication
from manageleave.models import Employee , Leave , LeaveRecord
from django.template import RequestContext
#from django.contrib.auth import authenticate, login
from django.contrib import auth
from django.contrib.auth.models import User
from datetime import datetime , date
from django.contrib.auth.decorators import login_required

def home(request): # render the login page that is the first page of our app
	if request.user.is_authenticated():
		return HttpResponseRedirect('/profile/')
	else:
		return render_to_response('index.html', context_instance=RequestContext(request))

def elogin(request):
	form = LoginForm()
	if (request.method != 'POST'):
		return render_to_response('home.html' , {'form':form} , context_instance=RequestContext(request))
	else:
		uname = request.POST['username']
		passwd = request.POST['password']
		user = auth.authenticate(username = uname , password = passwd )
		if user is not None:
			if user.is_active:
				auth.login(request,user) # here user is logged in as the username and password matched 
				return HttpResponseRedirect('/profile/')
	return render_to_response('home.html' , {'form':form} , context_instance=RequestContext(request))
			
def profile(request):
	return render_to_response('profile.html', context_instance=RequestContext(request))

@login_required
def requestleave(request):
	try:
		emp_leave = Leave.objects.get(emp_leave = request.user)
		total_leave = emp_leave.casual + emp_leave.sick + emp_leave.privilege + emp_leave.paternity
	except:
		error = "There is no entry for leave for you ! Please contact your HR Regarding that"
		return render_to_response('showmessage.html', {'error':error},context_instance=RequestContext(request))
	if (request.method == 'POST'):
		lform = LeaveApplication(request.POST)
		if lform.is_valid():
			new_snippet = lform.save(commit=False)
			#if request.user.is_authenticated():
			user = request.user
			emp = Employee.objects.get(name = user)
			new_snippet.employee = emp
			#no_of_application = LeaveRecord.objects.filter(employee = user , grant__null =  True).count()
			no_of_application = LeaveRecord.objects.filter(employee = user , grant__isnull = True).count()
			if no_of_application == 1:
				error = "sorry you have already submited the form "
				return render_to_response('showmessage.html' , {'error':error },context_instance=RequestContext(request))
			date_format = "%Y-%m-%d"
			start = datetime.strptime(request.POST['start_date'], date_format)
			end = datetime.strptime(request.POST['end_date'], date_format)
			now = datetime.now()
			diff = end - start
			leave_days = 0
			leave_days = diff.days
			check_leave(request , request.POST['leave_type'] , leave_days)
			if check_leave:
				new_snippet.days = leave_days
				new_snippet.application_date = now.date()
				new_snippet.save()
				return HttpResponseRedirect('/thanks/')
			else:
				lform = LeaveApplication()
				error = "Sorry You requested leave beyond your leave quota"
				return render_to_response('leaverequest.html' , {'form':lform , 'error':error}, context_instance=RequestContext(request))
		else:
			lform = LeaveApplication()
			return render_to_response('leaverequest.html' , {'form':lform , 'total_leave': total_leave , 'emp_leave' : emp_leave}, context_instance=RequestContext(request))
	else:
		lform = LeaveApplication()
		return render_to_response('leaverequest.html' , {'form':lform , 'total_leave': total_leave , 'emp_leave' : emp_leave}, context_instance=RequestContext(request))

@login_required
def leaveapplication(request):
	if request.user.is_authenticated():
		emp = Employee.objects.get(name = request.user)
		subordinates = Employee.objects.filter(manager = emp)
		empleaverecords = []
		message = ""
		for subordinate in subordinates:
			try:
				empleaverecord = LeaveRecord.objects.get(employee = subordinate.name , grant__isnull = True)
				empleaverecords.append(empleaverecord)
			except:
				message = "Your Team Working Fine , No leave Application"
		if len(empleaverecords) > 0:
				return render_to_response('subordinatesleave.html', {'empleaverecords':empleaverecords},context_instance=RequestContext(request))
		else:
			return render_to_response('showmessage.html', {'message':message},context_instance=RequestContext(request))

@login_required
def check_leave(request ,leave_type,leave_days):
	if request.user.is_authenticated():
		user = User.objects.get(username = request.user)
		try:
			emp_leave_left = Leave.objects.get(emp_leave = user)
		except:
			error = "There is no entry for leave for you ! Please contact your HR Regarding that"
		if leave_type == 'casual':
			leaveremaining = emp_leave_left.casual
		elif leave_type == 'sick':
			leaveremaining = emp_leave_left.sick
		elif leave_type == 'privilege':
			leaveremaining = emp_leave_left.privilege
		else:
			leaveremaining = emp_leave_left.paternity
		if int(leaveremaining) >= leave_days:
			return True
		else:
			return False
	else:
		return HttpResponseRedirect('/elogin/')

def thanks(request):
	return render_to_response('thanks.html', context_instance=RequestContext(request))

@login_required
def leave_allocated(request):
	if request.method == 'POST':
		if request.POST['grant'] == 'on':
			empname = request.POST['employee']
			subordinameasuser = ''
			subordinameasemp = ''
			subordinateasuser = User.objects.get(username = empname)
			subordinateasemp = Employee.objects.get(name = subordinateasuser)
			updateleavetable = Leave.objects.get(emp_leave = subordinateasemp)
			
			emp_leave_type = request.POST['leave_type']
			
			leave_allocated = int(request.POST['days'])
			if emp_leave_type == 'casual':
				updateleavetable.casual = updateleavetable.casual - leave_allocated
				updateleavetable.save()
			elif emp_leave_type == 'sick':
				updateleavetable.sick = updateleavetable.sick - leave_allocated
				updateleavetable.save()
			elif emp_leave_type == 'privilege':
				updateleavetable.privilege = updateleavetable.privilege - leave_allocated
				updateleavetable.save()
			else:
				updateleavetable.paternity = updateleavetable.paternity - leave_allocated
				updateleavetable.save()
			leaverecord = LeaveRecord.objects.get(employee = subordinateasemp , grant__isnull = True)
			leaverecord.days = request.POST['days']
			leaverecord.grant = True
			leaverecord.save()
	return render_to_response('thanks.html', context_instance=RequestContext(request))

@login_required
def leave_status(request):
	user = request.user
	user_as_emp = Employee.objects.get(name = user)
	try:
		emp_leave_stat = LeaveRecord.objects.filter(employee = user_as_emp , grant__isnull = False).order_by('application_date')[0]
		message = "Congrats you have been granted leave!!Your most recent granted leave is as Below."
		return render_to_response('leavestat.html',{'emp_leave_stat':emp_leave_stat, 'message' : message },context_instance=RequestContext(request))
	except IndexError:
		message = "Sorry we could not find any granted leave for you !!"
		return render_to_response('showmessage.html', {'message':message},context_instance=RequestContext(request))

def logout(request):
	auth.logout(request)
	return HttpResponseRedirect('/elogin/')
