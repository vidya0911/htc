from piston.handler import BaseHandler
from piston.utils import rc, throttle
from api.forms import Article
import os , difflib , json , openpyxl
from django.views.decorators.cache import cache_page
from django.core.cache import cache 
class TagArticleHandler(BaseHandler):
    allowed_methods = ['GET', 'POST']
    
    def create(self , request):
	para = request.data['para']
	#for taglist1
	taglist1 = cache.get('cachedtaglist1')
	parawords = str(para[0]).split()
	final_tags = []
	import pdb ; pdb.set_trace()
	for tag in taglist1:
	    matched_tag = difflib.get_close_matches(tag, parawords,1, 0.9)
	    if len(matched_tag) != 0:
		final_tags.append(tag)
	taged_para = {}
	#taged_para['para']=str(para[0])
	#for taglist2
	taglist2 = cache.get('cachedtaglist2')
	abbreviatedtags = taglist2[0].values()
	for k , v in taglist2[0].items():
	    if v in str(para[0]):
		final_tags.append(k)
	print final_tags
    
    def read(self,request):
	module_dir = os.path.dirname(__file__)
	tagfile = os.path.join(module_dir ,'tags/TAg-Master-list-synonyms.xlsx')
	tagfiles = openpyxl.load_workbook(tagfile)
	taglist_1_sheet = tagfiles.get_sheet_by_name(name='Tag list')
	taglist1 = []
	total_row_sheet_1 = taglist_1_sheet.get_highest_row()
	for col in range(7):
	    tag_type = taglist_1_sheet.cell(row = 0 , column=col).value
	    for row in range(total_row_sheet_1):
		if taglist_1_sheet.cell(row = row , column = col).value is not None:
		    tag = str(taglist_1_sheet.cell(row = row , column = col).value)
		    taglist1.append(tag)
		else:
		    break
	# For taglist2
	#stoptagfile = os.path.join(module_dir , 'tags/tags_stop_words.txt')
	taglist_2_sheet = tagfiles.get_sheet_by_name(name='synonyms')
	taglist2 = []
	abbreviated_tags = {}
	total_row_sheet_2 = taglist_2_sheet.get_highest_row()
	for row in range(1,taglist_2_sheet.get_highest_row()):
	    import pdb ; pdb.set_trace
	    k = str(taglist_2_sheet.cell(row = row , column = 0).value)
	    v = str(taglist_2_sheet.cell(row = row , column = 1).value)
	    abbreviated_tags[k] = v
	taglist2.append(abbreviated_tags)
	cache.set('cachedtaglist1' , taglist1, 3000)
	cache.set('cachedtaglist2' , taglist2, 3000)
	return taglist1 , taglist2

    
"""def tagpara(paragraph):
    taglist = cache.get('cachedtaglist1')
    final_tags = []
    parawords = str(paragraph[0])
    for tag in taglist:
	matched_tag = difflib.get_close_matches(tag, parawords, 2, 0.8)
	for tag in matched_tag:
	    if tag not in final_tags:
		final_tags.append(tag)
	taged_para = {}
	taged_para['para']=str(para[0])
	taged_para['tags']=final_tags
	taged_para_json = json.dumps(taged_para)
	return taged_para_json
#render a complete json of tags list based on the catogery and value
    def read(self,request):
	import pdb ; pdb.set_trace()
	module_dir = os.path.dirname(__file__)
	tagfile = os.path.join(module_dir ,'tags/TAg-Master-list-synonyms.xlsx')
	tagfiles = openpyxl.load_workbook(tagfile)
	taglist_1_sheet = tagfiles.get_sheet_by_name(name='Tag list')
	taglist1 = []
	tagscatogerised = {}
	total_row_sheet_1 = taglist_1_sheet.get_highest_row()
	for col in range(7):
	    tag_type = taglist_1_sheet.cell(row = 0 , column=col).value
	    typespecefictags=[]
	    for row in range(total_row_sheet_1):
		if taglist_1_sheet.cell(row = row , column = col).value is not None:
		    tag = str(taglist_1_sheet.cell(row = row , column = col).value)
		    typespecefictags.append(tag)
		else:
		    break
	    tagscatogerised[tag_type] = typespecefictags
	    taglist1.append(tagscatogerised)
	#tag = taglist1.cell(row = 100 , column=0).value
	cache.set('cachedtaglist1' , taglist1, 3000)
	return taglist1"""
