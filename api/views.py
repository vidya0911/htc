# Create your views here.
from tagarticles.forms import Article
from django.shortcuts import render_to_response , render
from django.template import RequestContext
from django.http import HttpResponseRedirect
import os , difflib

def postpara(request):
    module_dir = os.path.dirname(__file__)  #current directoryfile
    parafile = os.path.join(module_dir ,'tags/testpara.txt')
    paragraph = open(parafile, 'r').read()
    #para = {'para':paragraph}
    import pdb ; pdb.set_trace()
    request.post = paragraph
    return HttpResponseRedirect('/api/tagarticles/')
